<?php

use Phinx\Migration\AbstractMigration;

class CreatePermissionCategoryTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('auth_permission_category', ['id' => false]);
        $table
            ->addColumn('id', 'uuid', ['null' => false])
            ->addColumn('name', 'string', ['null' => false, 'limit' => 255 ])
            ->addIndex(['id'], ['unique' => true, 'name' => 'idx_pc_id'])
            ->addIndex(['name'], ['unique' => true, 'name' => 'idx_pc_name'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('auth_permission_category');
    }
}
