<?php
declare(strict_types=1);

namespace Authentication\Factory;

use Aura\Session\Session;
use Authentication\Form\LoginForm;
use Interop\Container\ContainerInterface;

final class LoginFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return LoginForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         *
         */
        $session = $container->get(Session::class);

        return new LoginForm('login', ['csrf' => $session->getCsrfToken()]);
    }
}