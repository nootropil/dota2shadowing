<?php
declare(strict_types=1);

namespace Authentication\Service;



use App\Core\Domain\Model\User\User;

final class Identity
{
    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $username;

    /**
     * Identity constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->username = $user->getUsername();
        $this->password = $user->getPasswordHash();
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }
}