<?php
declare(strict_types=1);

namespace Authentication;


final class ConfigProvider
{
    /**
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
            'routes' => $this->getRoutes(),
        ];
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [
            ],
            'factories' => [
                \Authentication\Service\MyAuthAdapter::class => \Authentication\Factory\MyAuthAdapterFactory::class,
                \Zend\Authentication\AuthenticationService::class => \Authentication\Factory\AuthenticationServiceFactory::class,
                \Authentication\Action\LoginAction::class => \Authentication\Factory\LoginActionFactory::class,
                \Authentication\Action\AuthAction::class => \Authentication\Factory\AuthActionFactory::class,
                \Authentication\Form\LoginForm::class => \Authentication\Factory\LoginFormFactory::class
            ],
        ];
    }

    /**
     * @return array
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'authentication' => ['src/Authentication/templates'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return [
            [
                'name' => 'login',
                'path' => '/login',
                'middleware' => [
                    \Authentication\Action\LoginAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
        ];
    }
}
