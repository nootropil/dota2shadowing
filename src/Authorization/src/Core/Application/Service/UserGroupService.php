<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Service;

use Authorization\Core\Application\Exception\ServiceException;
use Authorization\Core\Domain\Model\UserGroup;
use Authorization\Core\Domain\Repository\UserGroupRepository;
use Authorization\Core\Domain\Service\UserGroupInterface;

final class UserGroupService implements UserGroupInterface
{
    /**
     * @var UserGroupRepository
     */
    private $userGroupRepository;

    /**
     * UserGroupService constructor.
     * @param UserGroupRepository $userGroupRepository
     */
    public function __construct(
        UserGroupRepository $userGroupRepository
    )
    {
        $this->userGroupRepository = $userGroupRepository;
    }

    /**
     * @param string $userId
     * @param string $groupId
     * @throws ServiceException
     */
    public function createUserGroup(string $userId, string $groupId) : void
    {
        $userGroup = new UserGroup($userId, $groupId);

        if ($this->userGroupRepository->exists($userId, $groupId)) {
            throw new ServiceException("User already is in this group");
        }

        $this->userGroupRepository->add($userGroup);
    }

    /**
     * @param string $userId
     * @param string $groupId
     * @throws ServiceException
     */
    public function removeUserGroup(string $userId, string $groupId) : void
    {
        $userGroup = $this->userGroupRepository->fetch($userId, $groupId);

        if ($userGroup === null) {
            throw new ServiceException("UserGroup does not exists");
        }

        $this->userGroupRepository->remove($userGroup);
    }

    /**
     * @param string $userId
     * @return array
     */
    public function getUserPermissionsAsArray(string $userId) : array
    {
        $permissions = $this->userGroupRepository->fetchAllPermissionByUserAsArray($userId);

    }
}