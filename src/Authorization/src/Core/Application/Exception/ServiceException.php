<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Exception;

final class ServiceException extends \Exception
{
}