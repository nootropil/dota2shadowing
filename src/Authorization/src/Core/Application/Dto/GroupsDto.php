<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Dto;

use Authorization\Core\Domain\Model\Group;

final class GroupsDto
{
    /**
     * @var GroupDto[]
     */
    public $groups;

    /**
     * LanguagesDto constructor.
     * @param array $groups
     */
    public function __construct(array $groups)
    {
        /* @var $group Group */
        foreach ($groups as $group) {
            $this->groups[] = new GroupDto($group);
        }
    }
}