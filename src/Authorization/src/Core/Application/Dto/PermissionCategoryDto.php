<?php
declare(strict_types = 1);

namespace Authorization\Core\Application\Dto;

use Authorization\Core\Domain\Model\PermissionCategory;

final class PermissionCategoryDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * PermissionCategoriesDto constructor.
     * @param PermissionCategory $permissionCategory
     */
    public function __construct(PermissionCategory $permissionCategory)
    {
        $this->id = $permissionCategory->getId();
        $this->name = $permissionCategory->getName();
    }
}