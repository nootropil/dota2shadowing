<?php
declare(strict_types = 1);

namespace Authorization\Core\Infrastructure\Repository;

use App\Core\Infrastructure\Service\IdentityGenerator\Uuid4Generator;
use Authorization\Core\Domain\Model\Permission;
use Authorization\Core\Domain\Repository\PermissionRepository;
use PDO;

final class PdoPermissionRepository implements PermissionRepository
{
    const TABLE_NAME = 'auth_permission';

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var PermissionHydrator
     */
    private $hydrator;

    /**
     * PdoPermissionRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->hydrator = new PermissionHydrator();
    }

    /**
     * @param $id
     * @return Permission|null
     */
    public function fetch(string $id): ?Permission
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @return Permission[]
     */
    public function fetchAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $entities = [];
        foreach ($rows as $row) {
            array_push($entities, $this->toEntity($row));
        }
        return $entities;
    }

    /**
     * @param string $name
     * @return Permission|null
     */
    public function fetchByName(string $name): ?Permission
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE name = :name');
        $stmt->execute([':name' => $name]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function existsByName(string $name): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE name = :name');
        $stmt->execute([':name' => $name]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param string $description
     * @return Permission|null
     */
    public function fetchByDescription(string $description): ?Permission
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE description = :description');
        $stmt->execute([':description' => $description]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $description
     * @return bool
     */
    public function existsByDescription(string $description): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. self::TABLE_NAME . ' WHERE description = :description');
        $stmt->execute([':description' => $description]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param Permission $permission
     */
    public function add(Permission $permission): void
    {
        $stmt = $this->pdo->prepare('INSERT INTO ' . self::TABLE_NAME .
            ' (id, name, description, permission_category_id) VALUES (:id, :name, :description, :permission_category_id)');
        $stmt->bindValue(':id', $permission->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':name', $permission->getName(), PDO::PARAM_STR);
        $stmt->bindValue(':description', $permission->getDescription(), PDO::PARAM_STR);
        $stmt->bindValue(':permission_category_id', $permission->getPermissionCategoryId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param Permission $permission
     */
    public function save(Permission $permission): void
    {
        $stmt = $this->pdo->prepare('UPDATE ' . self::TABLE_NAME .
            ' SET description = :description, name = :name, permission_category_id = :permission_category_id WHERE id = :id');
        $stmt->bindValue(':id', $permission->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':description', $permission->getDescription(), PDO::PARAM_STR);
        $stmt->bindValue(':name', $permission->getName(), PDO::PARAM_STR);
        $stmt->bindValue(':permission_category_id', $permission->getPermissionCategoryId(), PDO::PARAM_STR);
        $stmt->execute();
    }


    /**
     * @param Permission $permission
     */
    public function remove(Permission $permission): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE id = :id');
        $stmt->bindValue(':id', $permission->getId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string
    {
        $uuidGenerator = new Uuid4Generator();
        return $uuidGenerator->generate();
    }

    /**
     * @param $result
     * @return Permission
     */
    private function toEntity(array $result): Permission
    {
        return $this->hydrator->hydrate($result);
    }
}