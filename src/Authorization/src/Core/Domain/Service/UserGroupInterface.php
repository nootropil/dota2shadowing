<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Service;

interface UserGroupInterface
{
    /**
     * @param string $userId
     * @param string $groupName
     */
    public function createUserGroup(string $userId, string $groupName) : void;

    /**
     * @param string $userId
     * @param string $groupName
     */
    public function removeUserGroup(string $userId, string $groupName) : void;

    /**
     * @param string $userId
     * @return array
     */
    public function getUserPermissionsAsArray(string $userId) : array;
}