<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Repository;

use Authorization\Core\Domain\Model\Group;

interface GroupRepository
{
    /**
     * @param $id
     * @return Group|null
     */
    public function fetch(string $id): ?Group;

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool;

    /**
     * @return Group[]
     */
    public function fetchAll(): array;

    /**
     * @param string $name
     * @return Group|null
     */
    public function fetchByName(string $name): ?Group;

    /**
     * @param string $name
     * @return bool
     */
    public function existsByName(string $name): bool;


    /**
     * @param string $description
     * @return Group|null
     */
    public function fetchByDescription(string $description): ?Group;

    /**
     * @param string $description
     * @return bool
     */
    public function existsByDescription(string $description): bool;

    /**
     * @param Group $group
     */
    public function save(Group $group): void;

    /**
     * @param Group $group
     */
    public function add(Group $group): void;

    /**
     * @param Group $group
     */
    public function remove(Group $group): void;

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string;
}