<?php
declare(strict_types = 1);

namespace Authorization\Core\Domain\Repository;

use DateTime;

interface LastUpdateRepository
{
    public function defineLastUpdate();

    /**
     * @return DateTime
     */
    public function fetchLastUpdate() : ?DateTime;
}