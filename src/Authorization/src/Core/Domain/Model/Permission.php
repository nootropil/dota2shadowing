<?php
declare(strict_types=1);

namespace Authorization\Core\Domain\Model;


final class Permission
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $permissionCategoryId;

    /**
     * Permission constructor.
     * @param string $id
     * @param string $description
     * @param string $name
     * @param string $permissionCategoryId
     */
    public function __construct(
        string $id,
        string $name,
        string $description,
        ?string $permissionCategoryId
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->permissionCategoryId = $permissionCategoryId;
    }

    /**
     * @param string $description
     */
    public function changeDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param string $permissionCategoryId
     */
    public function changePermissionCategoryId(string $permissionCategoryId): void
    {
        $this->permissionCategoryId = $permissionCategoryId;
    }

    /**
     * @param string $name
     */
    public function changeName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPermissionCategoryId(): ?string
    {
        return $this->permissionCategoryId;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'name' => $this->name,
            'permissionCategoryId' => $this->permissionCategoryId
        ];
    }
}