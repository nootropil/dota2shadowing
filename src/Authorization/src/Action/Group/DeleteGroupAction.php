<?php
declare(strict_types=1);

namespace Authorization\Action\Group;

use Authorization\Core\Domain\Service\GroupServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class DeleteGroupAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var GroupServiceInterface
     */
    private $groupService;

    /**
     * UpdateGroupAction constructor.
     * @param RouterInterface $router
     * @param GroupServiceInterface $groupService
     */
    public function __construct(
        RouterInterface $router,
        GroupServiceInterface $groupService
    )
    {
        $this->groupService = $groupService;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $this->groupService->removeGroup($request->getAttribute('id'));
        return new RedirectResponse($this->router->generateUri('authorization.groups'));
    }
}
