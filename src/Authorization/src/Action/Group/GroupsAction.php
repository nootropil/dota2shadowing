<?php
declare(strict_types=1);

namespace Authorization\Action\Group;

use Authorization\Core\Application\Dto\GroupsDto;
use Authorization\Core\Domain\Repository\GroupRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class GroupsAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * DeleteGroupAction constructor.
     * @param RouterInterface $router
     * @param GroupRepository $groupRepository
     * @param TemplateRendererInterface $template
     */
    public function __construct(
        RouterInterface $router,
        GroupRepository $groupRepository,
        TemplateRendererInterface $template
    )
    {
        $this->template = $template;
        $this->groupRepository = $groupRepository;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $groups = (new GroupsDto($this->groupRepository->fetchAll()))->groups;
        return new HtmlResponse($this->template->render('authorization::group/groups', [
            'groups' => $groups,
        ]));
    }
}
