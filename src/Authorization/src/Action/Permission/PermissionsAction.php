<?php
declare(strict_types=1);

namespace Authorization\Action\Permission;

use Authorization\Core\Application\Dto\PermissionsDto;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class PermissionsAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * DeletePermissionAction constructor.
     * @param RouterInterface $router
     * @param PermissionRepository $permissionRepository
     * @param TemplateRendererInterface $template
     */
    public function __construct(
        RouterInterface $router,
        PermissionRepository $permissionRepository,
        TemplateRendererInterface $template
    )
    {
        $this->template = $template;
        $this->permissionRepository = $permissionRepository;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $permissions = (new PermissionsDto($this->permissionRepository->fetchAll()))->permissions;
        return new HtmlResponse($this->template->render('authorization::permission/permissions', [
            'permissions' => $permissions,
        ]));
    }
}
