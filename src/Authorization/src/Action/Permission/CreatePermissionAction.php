<?php
declare(strict_types=1);

namespace Authorization\Action\Permission;

use Authorization\Core\Domain\Service\PermissionServiceInterface;
use Authorization\Form\CreatePermissionForm;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

class CreatePermissionAction
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var CreatePermissionForm
     */
    protected $form;

    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var PermissionServiceInterface
     */
    private $permissionService;

    /**
     * PermissionsAction constructor.
     * @param RouterInterface $router
     * @param TemplateRendererInterface|null $template
     * @param CreatePermissionForm $form
     * @param PermissionServiceInterface $permissionService
     */
    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template,
        CreatePermissionForm $form,
        PermissionServiceInterface $permissionService
    )
    {
        $this->permissionService = $permissionService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->permissionService->createPermission($this->form->getData());
            return new RedirectResponse($this->router->generateUri('authorization.permissions'));
        }
        return new HtmlResponse($this->template->render('authorization::permission/create', [
            'form' => $this->form,
        ]));
    }
}
