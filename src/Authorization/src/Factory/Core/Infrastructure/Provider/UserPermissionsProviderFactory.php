<?php
declare(strict_types=1);

namespace Authorization\Factory\Core\Infrastructure\Provider;

use Authorization\Core\Domain\Repository\UserGroupRepository;
use Authorization\Core\Infrastructure\Provider\UserPermissionsProvider;
use Interop\Container\ContainerInterface;
use Predis\Client;

final class UserPermissionsProviderFactory
{
    /**
     * @param ContainerInterface $container
     * @return UserPermissionsProvider
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $redisConnectionConfig = $config['redis_connection'];
        return new UserPermissionsProvider(
            $container->get(UserGroupRepository::class),
            new Client($redisConnectionConfig)
        );
    }
}