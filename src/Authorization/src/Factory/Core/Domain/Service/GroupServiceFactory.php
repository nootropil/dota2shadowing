<?php
declare(strict_types=1);

namespace Authorization\Factory\Core\Domain\Service;

use Interop\Container\ContainerInterface;
use Authorization\Core\Application\Service\GroupService;
use Authorization\Core\Domain\Repository\GroupRepository;

final class GroupServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return GroupService
     */
    public function __invoke(ContainerInterface $container)
    {
        return new GroupService(
            $container->get(GroupRepository::class)
        );
    }
}