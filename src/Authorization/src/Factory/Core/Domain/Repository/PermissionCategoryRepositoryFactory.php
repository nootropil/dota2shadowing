<?php
declare(strict_types=1);

namespace Authorization\Factory\Core\Domain\Repository;

use Interop\Container\ContainerInterface;
use PDO;
use Authorization\Core\Infrastructure\Repository\PdoPermissionCategoryRepository;

final class PermissionCategoryRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return PdoPermissionCategoryRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $connectionConfig = $config['db_connection'];
        $pdo = new PDO($connectionConfig);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return new PdoPermissionCategoryRepository($pdo);
    }
}