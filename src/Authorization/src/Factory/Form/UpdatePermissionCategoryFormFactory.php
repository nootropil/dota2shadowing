<?php
declare(strict_types=1);

namespace Authorization\Factory\Form;

use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Interop\Container\ContainerInterface;
use Authorization\Form\UpdatePermissionCategoryForm;
use Aura\Session\Session;

final class UpdatePermissionCategoryFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdatePermissionCategoryForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new UpdatePermissionCategoryForm(null, ['csrf' => $session->getCsrfToken()],  $container->get(PermissionCategoryRepository::class));
    }
}