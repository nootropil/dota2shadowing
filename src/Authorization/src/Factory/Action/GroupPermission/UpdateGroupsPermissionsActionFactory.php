<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\GroupPermission;

use Interop\Container\ContainerInterface;
use Authorization\Action\GroupPermission\UpdateGroupsPermissionsAction;
use Authorization\Core\Domain\Repository\GroupPermissionRepository;
use Authorization\Core\Domain\Repository\GroupRepository;
use Authorization\Core\Domain\Repository\PermissionCategoryRepository;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Authorization\Core\Domain\Service\GroupPermissionServiceInterface;
use Authorization\Form\UpdateGroupsPermissionsForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class UpdateGroupsPermissionsActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdateGroupsPermissionsAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new UpdateGroupsPermissionsAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(UpdateGroupsPermissionsForm::class),
            $container->get(GroupPermissionServiceInterface::class),
            $container->get(GroupPermissionRepository::class),
            $container->get(PermissionCategoryRepository::class),
            $container->get(PermissionRepository::class),
            $container->get(GroupRepository::class)
        );
    }
}