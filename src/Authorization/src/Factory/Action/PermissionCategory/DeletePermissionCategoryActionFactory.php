<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\PermissionCategory;

use Authorization\Action\PermissionCategory\DeletePermissionCategoryAction;
use Interop\Container\ContainerInterface;
use Authorization\Core\Domain\Service\PermissionCategoryServiceInterface;
use Zend\Expressive\Router\RouterInterface;

final class DeletePermissionCategoryActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return DeletePermissionCategoryAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DeletePermissionCategoryAction(
            $container->get(RouterInterface::class),
            $container->get(PermissionCategoryServiceInterface::class)
        );
    }
}