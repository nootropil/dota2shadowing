<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Permission;

use Interop\Container\ContainerInterface;
use Authorization\Action\Permission\UpdatePermissionAction;
use Authorization\Core\Domain\Repository\PermissionRepository;
use Authorization\Core\Domain\Service\PermissionServiceInterface;
use Authorization\Form\UpdatePermissionForm;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class UpdatePermissionActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return UpdatePermissionAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new UpdatePermissionAction(
            $container->get(RouterInterface::class),
            $container->get(TemplateRendererInterface::class),
            $container->get(UpdatePermissionForm::class),
            $container->get(PermissionServiceInterface::class),
            $container->get(PermissionRepository::class)
        );
    }
}