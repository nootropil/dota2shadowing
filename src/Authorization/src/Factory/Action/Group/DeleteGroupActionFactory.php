<?php
declare(strict_types=1);

namespace Authorization\Factory\Action\Group;

use Interop\Container\ContainerInterface;
use Authorization\Action\Group\DeleteGroupAction;
use Authorization\Core\Domain\Service\GroupServiceInterface;
use Zend\Expressive\Router\RouterInterface;

final class DeleteGroupActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return DeleteGroupAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new DeleteGroupAction(
            $container->get(RouterInterface::class),
            $container->get(GroupServiceInterface::class)
        );
    }
}