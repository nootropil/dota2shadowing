<?php
declare(strict_types=1);

namespace Authorization;


final class ConfigProvider
{
    /**
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
            'routes' => $this->getRoutes(),
        ];
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'factories' => [
                \Authorization\Core\Domain\Repository\GroupPermissionRepository::class => \Authorization\Factory\Core\Domain\Repository\GroupPermissionRepositoryFactory::class,
                \Authorization\Core\Domain\Repository\GroupRepository::class => \Authorization\Factory\Core\Domain\Repository\GroupRepositoryFactory::class,
                \Authorization\Core\Domain\Repository\PermissionCategoryRepository::class => \Authorization\Factory\Core\Domain\Repository\PermissionCategoryRepositoryFactory::class,
                \Authorization\Core\Domain\Repository\PermissionRepository::class => \Authorization\Factory\Core\Domain\Repository\PermissionRepositoryFactory::class,
                \Authorization\Core\Domain\Repository\UserGroupRepository::class => \Authorization\Factory\Core\Domain\Repository\UserGroupRepositoryFactory::class,

                \Authorization\Core\Domain\Service\GroupPermissionServiceInterface::class => \Authorization\Factory\Core\Domain\Service\GroupPermissionServiceFactory::class,
                \Authorization\Core\Domain\Service\GroupServiceInterface::class => \Authorization\Factory\Core\Domain\Service\GroupServiceFactory::class,
                \Authorization\Core\Domain\Service\PermissionCategoryServiceInterface::class => \Authorization\Factory\Core\Domain\Service\PermissionCategoryServiceFactory::class,
                \Authorization\Core\Domain\Service\PermissionServiceInterface::class => \Authorization\Factory\Core\Domain\Service\PermissionServiceFactory::class,

                \Authorization\Action\Group\CreateGroupAction::class => \Authorization\Factory\Action\Group\CreateGroupActionFactory::class,
                \Authorization\Action\Group\DeleteGroupAction::class => \Authorization\Factory\Action\Group\DeleteGroupActionFactory::class,
                \Authorization\Action\Group\GroupsAction::class => \Authorization\Factory\Action\Group\GroupsActionFactory::class,
                \Authorization\Action\Group\UpdateGroupAction::class => \Authorization\Factory\Action\Group\UpdateGroupActionFactory::class,
                \Authorization\Action\GroupPermission\UpdateGroupsPermissionsAction::class => \Authorization\Factory\Action\GroupPermission\UpdateGroupsPermissionsActionFactory::class,
                \Authorization\Action\Permission\CreatePermissionAction::class => \Authorization\Factory\Action\Permission\CreatePermissionActionFactory::class,
                \Authorization\Action\Permission\DeletePermissionAction::class => \Authorization\Factory\Action\Permission\DeletePermissionActionFactory::class,
                \Authorization\Action\Permission\PermissionsAction::class => \Authorization\Factory\Action\Permission\PermissionsActionFactory::class,
                \Authorization\Action\Permission\UpdatePermissionAction::class => \Authorization\Factory\Action\Permission\UpdatePermissionActionFactory::class,
                \Authorization\Action\PermissionCategory\CreatePermissionCategoryAction::class => \Authorization\Factory\Action\PermissionCategory\CreatePermissionCategoryActionFactory::class,
                \Authorization\Action\PermissionCategory\DeletePermissionCategoryAction::class => \Authorization\Factory\Action\PermissionCategory\DeletePermissionCategoryActionFactory::class,
                \Authorization\Action\PermissionCategory\PermissionsCategoriesAction::class => \Authorization\Factory\Action\PermissionCategory\PermissionsCategoriesActionFactory::class,
                \Authorization\Action\PermissionCategory\UpdatePermissionCategoryAction::class => \Authorization\Factory\Action\PermissionCategory\UpdatePermissionCategoryActionFactory::class,

                \Authorization\Form\CreateGroupForm::class => \Authorization\Factory\Form\CreateGroupFormFactory::class,
                \Authorization\Form\CreatePermissionCategoryForm::class => \Authorization\Factory\Form\CreatePermissionCategoryFormFactory::class,
                \Authorization\Form\CreatePermissionForm::class => \Authorization\Factory\Form\CreatePermissionFormFactory::class,
                \Authorization\Form\UpdateGroupForm::class => \Authorization\Factory\Form\UpdateGroupFormFactory::class,
                \Authorization\Form\UpdatePermissionCategoryForm::class => \Authorization\Factory\Form\UpdatePermissionCategoryFormFactory::class,
                \Authorization\Form\UpdateGroupsPermissionsForm::class => \Authorization\Factory\Form\UpdateGroupsPermissionsFormFactory::class,
                \Authorization\Form\UpdatePermissionForm::class => \Authorization\Factory\Form\UpdatePermissionFormFactory::class,

                \Authorization\Core\Infrastructure\Provider\UserPermissionsProviderInterface::class => \Authorization\Factory\Core\Infrastructure\Provider\UserPermissionsProviderFactory::class,

                /* Access actions*/
                \Authorization\Action\Access\ManageAuthorizationAction::class => \Authorization\Factory\Action\Access\ManageAuthorizationActionFactory::class,

            ],
        ];
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return [
            [
                'name' => 'authorization.group-create',
                'path' => '/authorization/group/create',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\Group\CreateGroupAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'authorization.group-delete',
                'path' => '/authorization/group/delete/{id}',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\Group\DeleteGroupAction::class
                ],
                'allowed_methods' => ['POST'],
            ],
            [
                'name' => 'authorization.group-update',
                'path' => '/authorization/group/update/{id}',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\Group\UpdateGroupAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'authorization.groups',
                'path' => '/authorization/group/groups',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\Group\GroupsAction::class
                ],
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'authorization.permission-create',
                'path' => '/authorization/permission/create',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\Permission\CreatePermissionAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'authorization.permission-delete',
                'path' => '/authorization/permission/delete/{id}',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\Permission\DeletePermissionAction::class
                ],
                'allowed_methods' => ['POST'],
            ],
            [
                'name' => 'authorization.permission-update',
                'path' => '/authorization/permission/update/{id}',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\Permission\UpdatePermissionAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'authorization.permissions',
                'path' => '/authorization/permission/permissions',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\Permission\PermissionsAction::class
                ],
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'authorization.permission-category-create',
                'path' => '/authorization/permission-category/create',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\PermissionCategory\CreatePermissionCategoryAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'authorization.permission-category-delete',
                'path' => '/authorization/permission-category/delete/{id}',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\PermissionCategory\DeletePermissionCategoryAction::class
                ],
                'allowed_methods' => ['POST'],
            ],
            [
                'name' => 'authorization.permission-category-update',
                'path' => '/authorization/permission-category/update/{id}',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\PermissionCategory\UpdatePermissionCategoryAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'authorization.permissions-categories',
                'path' => '/authorization/permissions-categories',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\PermissionCategory\PermissionsCategoriesAction::class
                ],
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'authorization.update-groups-permissions',
                'path' => '/authorization/group-permission/update-groups-permissions',
                'middleware' => [
                    \Authorization\Action\Access\ManageAuthorizationAction::class,
                    \Authorization\Action\GroupPermission\UpdateGroupsPermissionsAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'authorization' => ['src/Authorization/templates'],
            ],
        ];
    }
}
