<?php
declare(strict_types=1);

namespace Authorization\Form;

use Aura\Session\CsrfToken;
use Authorization\Core\Application\Dto\GroupDto;
use Authorization\Core\Application\Dto\PermissionDto;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

final class UpdateGroupsPermissionsForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var array
     */
    private $validators;

    /**
     * @param string|null $name
     * @param array|null $options
     */
    public function __construct($name = null, $options = [])
    {
        // we want to ignore the name passed
        parent::__construct('update-groups-permission', $options);


        $this->add([
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => [
                'value' => 'Update',
                'id' => 'submit-button',
            ],
        ]);

        $this->add([
            'name' => '_csrf',
            'type' => 'hidden',
            'attributes' => [
                'value' => $this->getOption('csrf')->getValue(),
            ],
        ]);
    }

    /**
     * @param GroupDto[] $groups
     * @param PermissionDto[] $permissions
     * @param array $gPArray
     */
    public function setCheckbox(array $groups, array $permissions, array $gPArray)
    {
        foreach ($groups as $group) {
            foreach ($permissions as $permission) {
                $this->add([
                    'name' => $group->id . '|' . $permission->id,
                    'type' => 'checkbox',
                    'options' => [
                        'required' => false,
                        'label' => $permission->name,
                        'unchecked_value' => 'no',
                        'checked_value' => 'yes',
                        'checked' => in_array($group->id . '|' . $permission->id, $gPArray)

                    ]
                ]);

                $this->validators[] =
                    [
                        'name' => $group->id . '|' . $permission->id,
                        'required' => false,
                        'filters' => [
                            ['name' => 'StripTags'],
                            ['name' => 'StringTrim'],
                        ],
                        'validators' => [
                            [
                                'name' => 'StringLength',
                                'options' => [
                                    'encoding' => 'UTF-8',
                                    'min' => 2,
                                    'max' => 3,
                                ]
                            ],
                        ]

                    ];
            }
        }

    }

    /**
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $this->validators[] = [
            'name' => '_csrf',
            'require' => true,
            'validators' => [
                [
                    'name' => 'callback',
                    'options' => [
                        'callback' => function ($value, $context, CsrfToken $csrf) {
                            if ($csrf->isValid($value)) {
                                return true;
                            }

                            return false;
                        },
                        'callbackOptions' => [
                            $this->getOption('csrf'),
                        ],
                        'message' => 'The form submitted did not originate from the expected site',
                    ],
                ],
            ],
        ];

        //var_dump($this->validators);die;
        return $this->validators;

    }
}
