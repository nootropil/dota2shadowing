<?php
declare(strict_types=1);

namespace App\Core\Application\Service\User;

use App\Core\Application\Event\AsyncEventBus;
use App\Core\Application\Event\User\ConformingEmailRequested;
use App\Core\Application\Event\User\RecoveringPasswordRequested;
use App\Core\Application\Event\User\UserRegistered;
use App\Core\Domain\Model\User\User;
use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Core\Domain\Model\UserToken\Types;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\User\UserRepository;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenRepository;
use App\Core\Infrastructure\Exception\ValidationException;

final class UserRegistrationService implements UserRegistrationServiceInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    /**
     * @var UserTokenReadRepository
     */
    private $userTokenReadRepository;

    /**
     * @var AsyncEventBus
     */
    private $asyncEventBus;

    /**
     * UserRegistrationService constructor.
     * @param UserRepository $userRepository
     * @param UserReadRepository $userReadRepository
     * @param UserTokenReadRepository $userTokenReadRepository
     * @param UserTokenRepository $userTokenRepository
     * @param AsyncEventBus $asyncEventBus
     */
    public function __construct(
        UserRepository $userRepository,
        UserReadRepository $userReadRepository,
        UserTokenReadRepository $userTokenReadRepository,
        UserTokenRepository $userTokenRepository,
        AsyncEventBus $asyncEventBus
    )
    {
        $this->asyncEventBus = $asyncEventBus;
        $this->userTokenRepository = $userTokenRepository;
        $this->userTokenReadRepository = $userTokenReadRepository;
        $this->userRepository = $userRepository;
        $this->userReadRepository = $userReadRepository;
    }


    /**
     * @param string $token
     * @throws ValidationException
     */
    public function confirmEmail(string $token): void
    {
        $this->validateToken($token, Types::EMAIL_CONFIRMATION);
        $token = $this->userTokenReadRepository->fetchByTokenAndType($token, Types::EMAIL_CONFIRMATION);
        $user = $this->userReadRepository->fetch($token->getUserId());
        $user->setRoleUser();
        $this->userRepository->save($user);
        $this->userTokenRepository->remove($token);
    }

    /**
     * @param string $token
     * @param string $password
     */
    public function recoverPassword(string $token, string $password): void
    {
        $this->validateToken($token, Types::PASSWORD_RECOVERING);
        $token = $this->userTokenReadRepository->fetchByTokenAndType($token, Types::PASSWORD_RECOVERING);
        $user = $this->userReadRepository->fetch($token->getUserId());
        $user->hashAndSavePassword($password);
        $this->userRepository->save($user);
        $this->userTokenRepository->remove($token);

    }

    /**
     * @param array $attributes
     * @throws ValidationException
     */
    public function register(array $attributes): void
    {
        if ($this->userReadRepository->existsByUsername($attributes['username'])) {
            throw new ValidationException('The user with this name already exists');
        }
        if ($this->userReadRepository->existsByEmail($attributes['email'])) {
            throw new ValidationException('The email is already registered');
        }
        $id = $this->userRepository->nextIdentity();
        $user = User::registerNew(
            $id,
            $attributes['username'],
            $attributes['email'],
            $attributes['password']
        );
        $this->userRepository->add($user);
        $this->asyncEventBus->handle(new UserRegistered($user->getId()));
    }

    /**
     * @param string $email
     * @throws ValidationException
     */
    public function requestToConfirmEmail(string $email): void
    {
        if (!$this->userReadRepository->existsByEmail($email)) {
            throw new ValidationException('The email does not exist');
        }
        $this->asyncEventBus->handle(new ConformingEmailRequested($email));
    }

    /**
     * @param string $email
     * @throws ValidationException
     */
    public function requestToRecoverPassword(string $email): void
    {
        if (!$this->userReadRepository->existsByEmail($email)) {
            throw new ValidationException('The email does not exist');
        }
        $this->asyncEventBus->handle(new RecoveringPasswordRequested($email));
    }

    /**
     * @param string $token
     * @param string $type
     * @throws ValidationException
     */
    private function validateToken(string $token, string $type) : void
    {
        if (strlen($token) < 5) {
            throw new ValidationException("Token is not valid");
        }
        if (!$this->userTokenReadRepository->existsByTokenAndType($token, $type)) {
            throw new ValidationException("Token does not exists");
        }
        $userToken = $this->userTokenReadRepository->fetchByTokenAndType($token, $type);
        if ($userToken->isExpired()) {
            throw new ValidationException("Token is expired");
        }
        if ($userToken->getType() === Types::EMAIL_CONFIRMATION) {
            $user = $this->userReadRepository->fetch($userToken->getUserId());
            if (!$user->isUnconfirmedUser()) {
                throw new ValidationException("Email is already confirmed");
            }
        }
    }
}