<?php
declare(strict_types=1);

namespace App\Core\Application\Service\User;

use App\Core\Domain\Model\User\User;
use App\Core\Domain\Model\UserToken\UserToken;
use App\Core\Domain\Service\User\UserNotificationServiceInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class UserNotificationService implements UserNotificationServiceInterface
{
    /**
     * @var TemplateRendererInterface
     */
    private $template;

    /**
     * @var Swift_Mailer
     */
    private $mailer;


    /**
     * SwiftMailerUserEmailNotificator constructor.
     * @param TemplateRendererInterface $template
     */
    public function __construct(TemplateRendererInterface $template)
    {
        $this->template = $template;
        $transport = (new Swift_SmtpTransport('smtp.yandex.ru', 465, 'ssl'))
            ->setUsername('your.big.green.daddy@yandex.ru')
            ->setPassword('123321wsx');
        $this->mailer = new Swift_Mailer($transport);
    }


    /**
     * @param UserToken $token
     * @param User $user
     */
    public function sendEmailConformationEmail(UserToken $token, User $user): void
    {
        $body = $this->template->render('email::email-confirmation', ['username' => $user->getUsername(), 'token' => $token->getToken()]);
        $message = new Swift_Message('Email conformation');
        $message->setFrom(['your.big.green.daddy@yandex.ru' => 'big.green.daddy']);
        $message->setTo([$user->getEmail()]);
        $message->setBody($body, 'text/html');
        $message->setCharset('utf-8');
        $this->mailer->send($message);
    }

    /**
     * @param UserToken $token
     * @param User $user
     */
    public function sendPasswordRecoveringEmail(UserToken $token, User $user): void
    {
        $body = $this->template->render('email::password-recovering', ['username' => $user->getUsername(), 'token' => $token->getToken()]);
        $message = new Swift_Message('Recovering password');
        $message->setFrom(['your.big.green.daddy@yandex.ru' => 'big.green.daddy']);
        $message->setTo([$user->getEmail()]);
        $message->setBody($body, 'text/html');
        $message->setCharset('utf-8');
        $this->mailer->send($message);
    }
}