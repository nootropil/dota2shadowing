<?php
declare(strict_types=1);

namespace App\Core\Application\Event;

use function Zelenin\Hydrator\createObjectWithoutConstructor;
use Zelenin\Hydrator\NamingStrategy\RawNamingStrategy;
use Zelenin\Hydrator\Strategy\ReflectionStrategy;
use Zelenin\Hydrator\StrategyHydrator;

class EventTransformer
{
    /**
     * @param Event $event
     * @return array
     */
    public function toArray(Event $event) : array
    {
        $hydrator = new StrategyHydrator(new ReflectionStrategy(), new RawNamingStrategy());
        $data = $hydrator->extract($event);
        $className = get_class($event);
        return ['class' => $className, 'payload' => $data];
    }

    /**
     * @param array $event
     * @return Event
     */
    public function fromArray(array $event) : Event
    {
        $class = $event['class'];
        $payload = $event['payload'];
        $hydrator = new StrategyHydrator(new ReflectionStrategy(), new RawNamingStrategy());
        $event = createObjectWithoutConstructor($class);
        return $hydrator->hydrate($event, $payload);
    }

}