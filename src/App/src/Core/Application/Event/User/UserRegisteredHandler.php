<?php
declare(strict_types = 1);

namespace App\Core\Application\Event\User;

use App\Core\Domain\Model\UserToken\Types;
use App\Core\Domain\Model\UserToken\UserToken;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenRepository;
use App\Core\Domain\Service\Notification\User\UserNotificationServiceInterface;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class UserRegisteredHandler implements Handler
{
    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    /**
     * @var UserNotificationServiceInterface
     */
    private $userNotificationService;

    /**
     * EmailConformationRequestedHandler constructor.
     * @param UserReadRepository $userReadRepository
     * @param UserTokenRepository $userTokenRepository
     * @param UserNotificationServiceInterface $userNotificationService
     */
    public function __construct(
        UserReadRepository $userReadRepository,
        UserTokenRepository $userTokenRepository,
        UserNotificationServiceInterface $userNotificationService
    )
    {
        $this->userReadRepository = $userReadRepository;
        $this->userTokenRepository = $userTokenRepository;
        $this->userNotificationService = $userNotificationService;
    }

    /**
     * @param object $event
     * @param Context $context
     * @return Context
     */
    public function __invoke($event, Context $context): Context
    {
        $user = $this->userReadRepository->fetch($event->getUserId());
        $userTokenId = $this->userTokenRepository->nextIdentity();
        $userToken = UserToken::createNew(
            $userTokenId,
            $user->getId(),
            Types::EMAIL_CONFIRMATION
        );
        $this->userTokenRepository->add($userToken);
        $this->userNotificationService->sendEmailConformationEmail($userToken, $user);
        return $context;
    }
}