<?php
declare(strict_types=1);

namespace App\Core\Application\Event\Middleware;

use App\Core\Application\Event\Event;
use App\Core\Application\Event\EventTransformer;
use App\Core\Infrastructure\Notification\RabbitMessageQueue;
use App\Core\Infrastructure\Notification\RabbitMqMessage;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Locator\Locator;
use Zelenin\MessageBus\MiddlewareBus\Middleware;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareDispatcher;

final class RabbitMqMiddleware implements Middleware
{
    /**
     * @var Locator
     */
    private $locator;

    /**
     * @param Locator $locator
     */
    public function __construct(Locator $locator)
    {
        $this->locator = $locator;
    }

    /**
     * @inheritdoc
     */
    public function __invoke($event, MiddlewareDispatcher $dispatcher): Context
    {
        /* @var $event Event */
        $eventSerializer = new EventTransformer();
        $eventArray = $eventSerializer->toArray($event);
        $rabbitMqMessage = new RabbitMqMessage(
            $this->getTopic($event),
            $this->getType($event),
            $eventArray
        );
        $messageQueue = new RabbitMessageQueue();
        $messageQueue->send($rabbitMqMessage);
        return $dispatcher($event);
    }

    /**
     * @param Event $event
     * @return string i.e. app.core.application.event.user.user_registered
     */
    private function getTopic(Event $event): string
    {
        $className = get_class($event);
        $className = str_replace('\\', '.', $className);
        $pattern = ['#(?<=(?:\p{Lu}))(\p{Lu}\p{Ll})#', '#(?<=(?:\p{Ll}|\p{Nd}))(\p{Lu})#'];
        $replacement = ['_\1', '_\1'];
        return mb_strtolower(preg_replace($pattern, $replacement, $className));
    }

    /**
     * @param Event $event
     * @return string
     */
    private function getType(Event $event): string
    {
        $className = get_class($event);
        return join('', array_slice(explode('\\', $className), -1));
    }

}