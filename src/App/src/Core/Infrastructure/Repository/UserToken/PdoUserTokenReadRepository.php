<?php
declare(strict_types = 1);

namespace App\Core\Infrastructure\Repository\UserToken;

use App\Core\Domain\Model\UserToken\UserToken;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use App\Core\Infrastructure\Repository\Hydrator;
use PDO;

final class PdoUserTokenReadRepository implements UserTokenReadRepository
{
    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @var Hydrator
     */
    private $hydrator;

    /**
     * PdoUserTokenReadRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->hydrator = new UserTokenHydrator();
        $this->pdo = $pdo;
    }

    /**
     * @param $id
     * @return UserToken
     */
    public function fetch(string $id): ?UserToken
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserTokenRepository::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserTokenRepository::TABLE_NAME . ' WHERE id = :id');
        $stmt->execute([':id' => $id]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @param string $token
     * @param string $type
     * @return UserToken|null
     */
    public function fetchByTokenAndType(string $token, string $type): ?UserToken
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserTokenRepository::TABLE_NAME . ' WHERE token = :token AND type = :type');
        $stmt->execute([':token' => $token, ':type' => $type]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param string $token
     * @param string $type
     * @return bool
     */
    public function existsByTokenAndType(string $token, string $type): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserTokenRepository::TABLE_NAME . ' WHERE token = :token AND type = :type');
        $stmt->execute([':token' => $token, ':type' => $type]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        return boolval($row);
    }

    /**
     * @return UserToken[]
     */
    public function fetchAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM '. PdoUserTokenRepository::TABLE_NAME);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $userTokens = [];
        foreach ($rows as $row) {
            array_push($userTokens, $this->toEntity($row));
        }
        return $userTokens;
    }

    /**
     * @param $result
     * @return UserToken
     */
    private function toEntity(array $result): UserToken
    {
        return $this->hydrator->hydrate($result);
    }

}