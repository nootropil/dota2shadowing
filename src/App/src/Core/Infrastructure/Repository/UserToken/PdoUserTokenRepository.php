<?php
declare(strict_types = 1);

namespace App\Core\Infrastructure\Repository\UserToken;

use App\Core\Domain\Model\UserToken\UserToken;
use App\Core\Domain\Repository\UserToken\UserTokenRepository;
use App\Core\Infrastructure\Service\IdentityGenerator\Uuid4Generator;
use PDO;

final class PdoUserTokenRepository implements UserTokenRepository
{
    const TABLE_NAME = 'public.user_token';

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * PdoUserRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param UserToken $userToken
     */
    public function add(UserToken $userToken): void
    {
        $stmt = $this->pdo->prepare('INSERT INTO ' . self::TABLE_NAME .
            ' (id, user_id, token, type, updated) VALUES (:id, :user_id, :token, :type, :updated)');
        $stmt->bindValue(':id', $userToken->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':user_id', $userToken->getUserId(), PDO::PARAM_STR);
        $stmt->bindValue(':token', $userToken->getToken(), PDO::PARAM_STR);
        $stmt->bindValue(':type', $userToken->getType(), PDO::PARAM_STR);
        $stmt->bindValue(':created', $userToken->getCreated()->format('Y-m-d H:i:s'), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param UserToken $userToken
     */
    public function save(UserToken $userToken): void
    {
        $stmt = $this->pdo->prepare('UPDATE ' . self::TABLE_NAME .
            ' SET user_id = :user_id, token = :token, type = :type, created = :created WHERE id = :id');
        $stmt->bindValue(':id', $userToken->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':user_id', $userToken->getUserId(), PDO::PARAM_STR);
        $stmt->bindValue(':token', $userToken->getToken(), PDO::PARAM_STR);
        $stmt->bindValue(':type', $userToken->getType(), PDO::PARAM_STR);
        $stmt->bindValue(':created', $userToken->getCreated()->format('Y-m-d H:i:s'), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param UserToken $userToken
     */
    public function remove(UserToken $userToken): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE id = :id');
        $stmt->bindValue(':id', $userToken->getId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string
    {
        $uuidGenerator = new Uuid4Generator();
        return $uuidGenerator->generate();
    }
}