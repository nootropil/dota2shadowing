<?php
declare(strict_types = 1);

namespace App\Core\Infrastructure\Repository;

interface Hydrator
{
    /**
     * Return some entity
     *
     * @param array $columns
     */
    public function hydrate(array $columns);
}