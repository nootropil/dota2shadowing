<?php
declare(strict_types = 1);

namespace App\Core\Infrastructure\Repository\User;

use App\Core\Domain\Model\User\User;
use App\Core\Domain\Repository\User\UserRepository;
use App\Core\Infrastructure\Service\IdentityGenerator\Uuid4Generator;
use PDO;

final class PdoUserRepository implements UserRepository
{
    const TABLE_NAME = 'public.user';

    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * PdoUserRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param User $user
     */
    public function add(User $user): void
    {
        $updated = $user->getUpdated() !== null ? $user->getUpdated()->format('Y-m-d H:i:s') : null;
        $stmt = $this->pdo->prepare('INSERT INTO ' . self::TABLE_NAME .
            ' (id, username, email, password_hash, role, status, created, updated) 
            VALUES (:id, :username, :email, :password_hash, :role, :status, :created, :updated)');
        $stmt->bindValue(':id', $user->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':username', $user->getUsername(), PDO::PARAM_STR);
        $stmt->bindValue(':email', $user->getEmail(), PDO::PARAM_STR);
        $stmt->bindValue(':password_hash', $user->getPasswordHash(), PDO::PARAM_STR);
        $stmt->bindValue(':role', $user->getRole(), PDO::PARAM_STR);
        $stmt->bindValue(':status', $user->getStatus(), PDO::PARAM_STR);
        $stmt->bindValue(':created', $user->getCreated()->format('Y-m-d H:i:s'), PDO::PARAM_STR);
        $stmt->bindValue(':updated', $updated, PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param User $user
     */
    public function save(User $user): void
    {
        $stmt = $this->pdo->prepare('UPDATE ' . self::TABLE_NAME .
            ' SET username = :username, email = :email, password_hash = :password_hash, role = :role, status = :status, created = :created, updated = :updated WHERE id = :id');
        $stmt->bindValue(':id', $user->getId(), PDO::PARAM_STR);
        $stmt->bindValue(':username', $user->getUsername(), PDO::PARAM_STR);
        $stmt->bindValue(':email', $user->getEmail(), PDO::PARAM_STR);
        $stmt->bindValue(':password_hash', $user->getPasswordHash(), PDO::PARAM_STR);
        $stmt->bindValue(':role', $user->getRole(), PDO::PARAM_STR);
        $stmt->bindValue(':status', $user->getStatus(), PDO::PARAM_STR);
        $stmt->bindValue(':created', $user->getCreated()->format('Y-m-d H:i:s'), PDO::PARAM_STR);
        $stmt->bindValue(':updated', (new \DateTime('now'))->format('Y-m-d H:i:s'), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * @param User $user
     */
    public function remove(User $user): void
    {
        $stmt = $this->pdo->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE id = :id');
        $stmt->bindValue(':id', $user->getId(), PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity(): string
    {
        $uuidGenerator = new Uuid4Generator();
        return $uuidGenerator->generate();
    }
}