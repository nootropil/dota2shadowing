<?php

namespace App\Core\Domain\Contract;


interface HasId
{
    public function getId(): string;
}