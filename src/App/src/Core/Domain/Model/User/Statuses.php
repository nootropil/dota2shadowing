<?php
declare(strict_types = 1);

namespace App\Core\Domain\Model\User;

final class Statuses
{
    const ACTIVE = 'active';
    const BLOCKED = 'blocked';
    const UNCONFIRMED = 'blocked';
}