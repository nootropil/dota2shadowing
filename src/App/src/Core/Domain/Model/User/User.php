<?php
declare(strict_types = 1);

namespace App\Core\Domain\Model\User;

use App\Core\Domain\Contract\HasId;
use DateTime;

final class User implements HasId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $passwordHash;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime|null
     */
    private $updated;

    /**
     * User constructor.
     * @param string $id
     * @param string $username
     * @param string $email
     * @param string $status
     * @param DateTime $created
     */
    private function __construct(
        string $id,
        string $username,
        string $email,
        string $status,
        DateTime $created
    )
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->status = $status;
        $this->created = $created;
    }

    /**
     * @param string $password
     */
    public function hashAndSavePassword(string $password) : void
    {
        $this->passwordHash = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @param string $id
     * @param string $username
     * @param string $email
     * @param string $password
     * @return User
     */
    public static function registerNew(
        string $id,
        string $username,
        string $email,
        string $password
    ): User
    {
        $user = new User(
            $id,
            $username,
            $email,
            Statuses::ACTIVE,
            new DateTime('now')
        );
        $user->hashAndSavePassword($password);
        return $user;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $role
     */
    public function changeUserData(
        string $username,
        string $email
    ): void
    {
        $this->username = $username;
        $this->email = $email;
    }

    public function setRoleUser(): void
    {
        $this->role = Roles::USER;
    }

    /**
     * @param string $status
     */
    public function changeStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status == Statuses::ACTIVE;
    }

    /**
     * @return bool
     */
    public function isBlocked(): bool
    {
        return $this->status == Statuses::BLOCKED;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    public function isUnconfirmedUser(): bool
    {
        return $this->status === Statuses::UNCONFIRMED;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdated(): ?DateTime
    {
        return $this->updated;
    }
}