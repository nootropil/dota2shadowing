<?php
declare(strict_types=1);

namespace App\Core\Domain\Model\UserToken;

use App\Core\Domain\Contract\HasId;
use DateTime;

final class UserToken implements HasId
{
    const EXPIRE_TOKEN = 3600;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $userId;
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $type;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * UserToken constructor.
     * @param string $id
     * @param string $userId
     * @param string $token
     * @param string $type
     * @param DateTime $created
     */
    private function __construct(
        string $id,
        string $userId,
        string $token,
        string $type,
        DateTime $created
    )
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->type = $type;
        $this->token = $token;
        $this->created = $created;
    }

    /**
     * @param string $id
     * @param string $userId
     * @param string $type
     * @return UserToken
     */
    public static function createNew(
        string $id,
        string $userId,
        string $type
    ): UserToken
    {
        $userToken = new UserToken(
            $id,
            $userId,
            bin2hex(random_bytes(64)),
            $type,
            new DateTime('now')
        );
        return $userToken;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
       return $this->getCreated()->getTimestamp() + self::EXPIRE_TOKEN <= (new \DateTime('now'))->getTimestamp();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }


    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }
}