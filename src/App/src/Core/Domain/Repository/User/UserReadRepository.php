<?php
declare(strict_types = 1);

namespace App\Core\Domain\Repository\User;

use App\Core\Domain\Model\User\User;

interface UserReadRepository
{
    /**
     * @param $id
     * @return User|null
     */
    public function fetch(string $id): ?User;

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id): bool;

    /**
     * @return User[]
     */
    public function fetchAll(): array;

    /**
     * @param $email
     * @return User|null
     */
    public function fetchByEmail(string $email): ?User;

    /**
     * @param string $email
     * @return bool
     */
    public function existsByEmail(string $email): bool;

    /**
     * @param $username
     * @return User|null
     */
    public function fetchByUsername(string $username): ?User;

    /**
     * @param string $username
     * @return bool
     */
    public function existsByUsername(string $username): bool;
}