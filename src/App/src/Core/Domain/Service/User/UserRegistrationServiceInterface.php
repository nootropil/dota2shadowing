<?php
declare(strict_types = 1);

namespace App\Core\Domain\Service\User;


interface UserRegistrationServiceInterface
{
    /**
     * @param string $token
     */
    public function confirmEmail(string $token): void;

    /**
     * @param string $token
     * @param string $password
     */
    public function recoverPassword(string $token, string $password): void;

    /**
     * @param array $attributes
     */
    public function register(array $attributes): void;

    /**
     * @param string $email
     */
    public function requestToConfirmEmail(string $email): void;

    /**
     * @param string $email
     */
    public function requestToRecoverPassword(string $email): void;
}