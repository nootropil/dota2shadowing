<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Form\User;

use App\Presentation\Form\User\PasswordRecoveringForm;
use Aura\Session\Session;
use Interop\Container\ContainerInterface;

final class PasswordRecoveringFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return PasswordRecoveringForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);
        return new PasswordRecoveringForm('password-recovering', ['csrf' => $session->getCsrfToken()]);
    }
}