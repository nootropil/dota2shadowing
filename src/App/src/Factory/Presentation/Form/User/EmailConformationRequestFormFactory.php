<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Form\User;

use App\Presentation\Form\User\EmailConformationRequestForm;
use Aura\Session\Session;
use Interop\Container\ContainerInterface;

final class EmailConformationRequestFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return EmailConformationRequestForm
     */
    public function __invoke(ContainerInterface $container)
    {
        /**
         * @var Session $session
         */
        $session = $container->get(Session::class);

        return new EmailConformationRequestForm('email-confirmation-request', ['csrf' => $session->getCsrfToken()]);
    }
}