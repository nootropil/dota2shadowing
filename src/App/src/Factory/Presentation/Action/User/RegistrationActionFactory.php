<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Presentation\Action\User\RegistrationAction;
use App\Presentation\Form\User\RegistrationForm;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class RegistrationActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return RegistrationAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);
        $template = ($container->has(TemplateRendererInterface::class))
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $form = $container->get(RegistrationForm::class);
        $userRegistrationServiceInterface = $container->get(UserRegistrationServiceInterface::class);

        return new RegistrationAction($router, $template, $form, $userRegistrationServiceInterface);
    }
}
