<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Presentation\Action\User\EmailConfirmationRequestAction;
use App\Presentation\Form\User\EmailConformationRequestForm;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class EmailConfirmationRequestActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return EmailConfirmationRequestAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);
        $template = $container->get(TemplateRendererInterface::class);
        $form = $container->get(EmailConformationRequestForm::class);
        $userRegistrationServiceInterface = $container->get(UserRegistrationServiceInterface::class);

        return new EmailConfirmationRequestAction($router, $template, $form, $userRegistrationServiceInterface);
    }
}
