<?php
declare(strict_types=1);

namespace App\Factory\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Presentation\Action\User\EmailConfirmationAction;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;

final class EmailConfirmationActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return EmailConfirmationAction
     */
    public function __invoke(ContainerInterface $container)
    {
        return new EmailConfirmationAction(
            $container->get(RouterInterface::class),
            $container->get(UserRegistrationServiceInterface::class)
        );
    }
}
