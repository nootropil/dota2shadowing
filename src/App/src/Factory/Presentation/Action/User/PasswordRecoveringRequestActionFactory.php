<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Presentation\Action\User\PasswordRecoveringRequestAction;
use App\Presentation\Form\User\PasswordRecoveringRequestForm;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class PasswordRecoveringRequestActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return PasswordRecoveringRequestAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);
        $template = $container->get(TemplateRendererInterface::class);
        $form = $container->get(PasswordRecoveringRequestForm::class);
        $userRegistrationServiceInterface = $container->get(UserRegistrationServiceInterface::class);

        return new PasswordRecoveringRequestAction($router, $template, $form, $userRegistrationServiceInterface);
    }
}
