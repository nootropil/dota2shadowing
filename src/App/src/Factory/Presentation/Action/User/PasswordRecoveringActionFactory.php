<?php
declare(strict_types = 1);

namespace App\Factory\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Presentation\Action\User\PasswordRecoveringAction;
use App\Presentation\Form\User\PasswordRecoveringForm;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class PasswordRecoveringActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return PasswordRecoveringAction
     */
    public function __invoke(ContainerInterface $container)
    {
        $router = $container->get(RouterInterface::class);
        $template = $container->get(TemplateRendererInterface::class);
        $form = $container->get(PasswordRecoveringForm::class);
        $userRegistrationServiceInterface = $container->get(UserRegistrationServiceInterface::class);

        return new PasswordRecoveringAction($router, $template, $form, $userRegistrationServiceInterface);
    }
}
