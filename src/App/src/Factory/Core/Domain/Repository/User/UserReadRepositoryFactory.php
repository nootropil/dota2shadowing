<?php
declare(strict_types=1);

namespace App\Factory\Core\Domain\Repository\User;

use App\Core\Infrastructure\Repository\User\PdoUserReadRepository;
use Interop\Container\ContainerInterface;
use PDO;

final class UserReadRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     * @return PdoUserReadRepository
     */
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');
        $connectionConfig = $config['db_connection'];
        $pdo = new PDO($connectionConfig);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return new PdoUserReadRepository($pdo);
    }
}