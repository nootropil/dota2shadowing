<?php
declare(strict_types = 1);

namespace App\Factory\Core\Domain\Service\User;

use App\Core\Application\Event\AsyncEventBus;
use App\Core\Application\Service\User\UserRegistrationService;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\User\UserRepository;
use App\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenRepository;
use Interop\Container\ContainerInterface;

final class UserRegistrationServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return UserRegistrationService
     */
    public function __invoke(ContainerInterface $container)
    {
        return new UserRegistrationService(
            $container->get(UserRepository::class),
            $container->get(UserReadRepository::class),
            $container->get(UserTokenReadRepository::class),
            $container->get(UserTokenRepository::class),
            $container->get(AsyncEventBus::class)
        );
    }

}