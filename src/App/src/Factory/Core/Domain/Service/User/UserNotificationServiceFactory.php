<?php
declare(strict_types = 1);

namespace App\Factory\Core\Domain\Service\User;

use App\Core\Application\Service\User\UserNotificationService;
use Interop\Container\ContainerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class UserNotificationServiceFactory
{
    /**
     * @param ContainerInterface $container
     * @return UserNotificationService
     */
    public function __invoke(ContainerInterface $container)
    {
        $template = $container->get(TemplateRendererInterface::class);
        return new UserNotificationService($template);
    }
}