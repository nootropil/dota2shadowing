<?php
declare(strict_types = 1);

namespace App\Factory\Core\Application\Event\User;

use App\Core\Application\Event\User\RecoveringPasswordRequestedHandler;
use App\Core\Domain\Repository\User\UserReadRepository;
use App\Core\Domain\Repository\UserToken\UserTokenRepository;
use App\Core\Domain\Service\Notification\User\UserNotificationServiceInterface;
use Interop\Container\ContainerInterface;

final class RecoveringPasswordRequestedHandlerFactory
{
    /**
     * @param ContainerInterface $container
     * @return RecoveringPasswordRequestedHandler
     */
    public function __invoke(ContainerInterface $container)
    {
        $userReadRepository = $container->get(UserReadRepository::class);
        $userTokenRepository = $container->get(UserTokenRepository::class);
        $userEmailNotificator = $container->get(UserNotificationServiceInterface::class);
        return new RecoveringPasswordRequestedHandler($userReadRepository, $userTokenRepository, $userEmailNotificator);
    }
}