<?php
declare(strict_types=1);

namespace App;


final class ConfigProvider
{
    /**
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates' => $this->getTemplates(),
            'console' => $this->getConsole(),
            'routes' => $this->getRoutes(),
        ];
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'factories' => [
                Core\Application\Event\AsyncEventBus::class => Factory\Core\Application\Event\AsyncEventBusFactory::class,
                Core\Application\Event\EventBus::class => Factory\Core\Application\Event\EventBusFactory::class,

                /* Repositories */
                Core\Domain\Repository\User\UserReadRepository::class => Factory\Core\Domain\Repository\User\UserReadRepositoryFactory::class,
                Core\Domain\Repository\User\UserRepository::class => Factory\Core\Domain\Repository\User\UserRepositoryFactory::class,
                Core\Domain\Repository\UserToken\UserTokenReadRepository::class => Factory\Core\Domain\Repository\UserToken\UserTokenReadRepositoryFactory::class,
                Core\Domain\Repository\UserToken\UserTokenRepository::class => Factory\Core\Domain\Repository\UserToken\UserTokenRepositoryFactory::class,

                /* Events */
                Core\Application\Event\User\UserRegisteredHandler::class => Factory\Core\Application\Event\User\UserRegisteredHandlerFactory::class,
                Core\Application\Event\User\RecoveringPasswordRequestedHandler::class => Factory\Core\Application\Event\User\RecoveringPasswordRequestedHandlerFactory::class,
                Core\Application\Event\User\ConformingEmailRequestedHandler::class => Factory\Core\Application\Event\User\ConformingEmailRequestedHandlerFactory::class,

                /* Actions */
                Presentation\Action\User\RegistrationAction::class => Factory\Presentation\Action\User\RegistrationActionFactory::class,
                Presentation\Action\User\EmailConfirmationAction::class => Factory\Presentation\Action\User\EmailConfirmationActionFactory::class,
                Presentation\Action\User\EmailConfirmationRequestAction::class => Factory\Presentation\Action\User\EmailConfirmationRequestActionFactory::class,
                Presentation\Action\User\PasswordRecoveringAction::class => Factory\Presentation\Action\User\PasswordRecoveringActionFactory::class,
                Presentation\Action\User\PasswordRecoveringRequestAction::class => Factory\Presentation\Action\User\PasswordRecoveringRequestActionFactory::class,
                Presentation\Action\Common\IndexAction::class => Factory\Presentation\Action\Common\IndexActionFactory::class,

                /* Forms */
                Presentation\Form\User\RegistrationForm::class => Factory\Presentation\Form\User\RegistrationFormFactory::class,
                Presentation\Form\User\EmailConformationRequestForm::class => Factory\Presentation\Form\User\EmailConformationRequestFormFactory::class,
                Presentation\Form\User\PasswordRecoveringForm::class => Factory\Presentation\Form\User\PasswordRecoveringFormFactory::class,
                Presentation\Form\User\PasswordRecoveringRequestForm::class => Factory\Presentation\Form\User\PasswordRecoveringRequestFormFactory::class,

                /* Services */
                Core\Domain\Service\User\UserNotificationServiceInterface::class => Factory\Core\Domain\Service\User\UserNotificationServiceFactory::class,
                Core\Domain\Service\User\UserRegistrationServiceInterface::class => Factory\Core\Domain\Service\User\UserRegistrationServiceFactory::class,


                /*Console commands */
                Command\ListenEventCommand::class => Factory\Command\ListenEventCommandFactory::class,
            ],
        ];
    }

    /**
     * @return array
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'email' => ['src/App/templates/email'],
                'app' => ['src/App/templates'],
                'error' => ['src/App/templates/error'],
                'layout' => ['src/App/templates/layout'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function getConsole(): array
    {
        return [
            'commands' => [
                Command\ListenEventCommand::class,
            ],
        ];
    }


    /**
     * @return array
     */
    public function getRoutes() : array
    {
        return [
            [
                'name' => 'registration',
                'path' => '/user/registration',
                'middleware' => [
                    \Authentication\Action\AuthAction::class,
                    \App\Presentation\Action\User\RegistrationAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'index',
                'path' => '/',
                'middleware' => [
                    \App\Presentation\Action\Common\IndexAction::class
                ],
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'email-conformation',
                'path' => '/user/email-conformation/{token}',
                'middleware' => [
                    \App\Presentation\Action\User\EmailConfirmationAction::class
                ],
                'allowed_methods' => ['GET'],
            ],
            [
                'name' => 'password-recovering',
                'path' => '//user/password-recovering/{token}',
                'middleware' => [
                    \App\Presentation\Action\User\PasswordRecoveringAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'email-conformation-request',
                'path' => '/user/email-conformation-request',
                'middleware' => [
                    \App\Presentation\Action\User\EmailConfirmationRequestAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
            [
                'name' => 'password-recovering-request',
                'path' => '/user/password-recovering-request',
                'middleware' => [
                    \App\Presentation\Action\User\PasswordRecoveringRequestAction::class
                ],
                'allowed_methods' => ['GET', 'POST'],
            ],
        ];
    }
}
