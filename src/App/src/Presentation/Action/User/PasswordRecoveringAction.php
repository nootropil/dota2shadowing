<?php
declare(strict_types=1);

namespace App\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Presentation\Form\User\PasswordRecoveringForm;
use Aura\Session\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;

class PasswordRecoveringAction
{

    /**
     * @var Router\RouterInterface
     */
    protected $router;

    /**
     * @var PasswordRecoveringForm
     */
    protected $form;

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * @var UserRegistrationServiceInterface
     */
    private $userRegistrationService;

    /**
     * PasswordRecoveringAction constructor.
     * @param Router\RouterInterface $router
     * @param Template\TemplateRendererInterface|null $template
     * @param PasswordRecoveringForm $form
     * @param UserRegistrationServiceInterface $userRegistrationService
     */
    public function __construct(
        Router\RouterInterface $router,
        Template\TemplateRendererInterface $template = null,
        PasswordRecoveringForm $form,
        UserRegistrationServiceInterface$userRegistrationService
    )
    {
        $this->userRegistrationService = $userRegistrationService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        /**
         * @var Session $session
         */
        $session = $request->getAttribute('session');
        $data = $request->getParsedBody();
        $this->form->setData($data);
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->userRegistrationService->recoverPassword($request->getAttribute('token'), $data['password']);
            $session->getSegment('App\Action')->setFlash(
                'flash',
                ['type' => 'success', 'message' => 'Password is changed']
            );
            return new RedirectResponse($this->router->generateUri('index'));
        }
        return new HtmlResponse($this->template->render('app::user/password-recovering', [
            'form' => $this->form,
        ]));
    }
}
