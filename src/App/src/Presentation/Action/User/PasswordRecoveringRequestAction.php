<?php
declare(strict_types=1);

namespace App\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Presentation\Form\User\PasswordRecoveringRequestForm;
use Aura\Session\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;

class PasswordRecoveringRequestAction
{
    /**
     * @var UserRegistrationServiceInterface
     */
    private $userRegistrationService;

    /**
     * @var Router\RouterInterface
     */
    protected $router;

    /**
     * @var PasswordRecoveringRequestForm
     */
    protected $form;

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * PasswordRecoveringRequestAction constructor.
     * @param Router\RouterInterface $router
     * @param Template\TemplateRendererInterface|null $template
     * @param PasswordRecoveringRequestForm $form
     * @param UserRegistrationServiceInterface $userRegistrationService
     */
    public function __construct(
        Router\RouterInterface $router,
        Template\TemplateRendererInterface $template = null,
        PasswordRecoveringRequestForm $form,
        UserRegistrationServiceInterface $userRegistrationService
    )
    {

        $this->userRegistrationService = $userRegistrationService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        /**
         * @var Session $session
         */
        $session = $request->getAttribute('session');
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            /* @var $email string*/
            $email = $request->getParsedBody()['email'];
            $this->userRegistrationService->requestToRecoverPassword($email);
            $session->getSegment('App\Action')->setFlash(
                'flash',
                ['type' => 'success', 'message' => 'Link to changed your password is sent to your email']
            );
            return new RedirectResponse($this->router->generateUri('index'));
        }
        return new HtmlResponse($this->template->render('app::user/email-confirmation-request', [
            'form' => $this->form,
        ]));
    }
}
