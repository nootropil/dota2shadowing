<?php
declare(strict_types=1);

namespace App\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Presentation\Form\User\EmailConformationRequestForm;
use App\Presentation\Form\User\RegistrationForm;
use Aura\Session\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;

class EmailConfirmationRequestAction
{
    /**
     * @var UserRegistrationServiceInterface
     */
    private $userRegistrationService;

    /**
     * @var Router\RouterInterface
     */
    protected $router;

    /**
     * @var EmailConformationRequestForm
     */
    protected $form;

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * EmailConfirmationRequestAction constructor.
     * @param Router\RouterInterface $router
     * @param Template\TemplateRendererInterface|null $template
     * @param EmailConformationRequestForm $form
     * @param UserRegistrationServiceInterface $userRegistrationService
     */
    public function __construct(
        Router\RouterInterface $router,
        Template\TemplateRendererInterface $template = null,
        EmailConformationRequestForm $form,
        UserRegistrationServiceInterface $userRegistrationService
    )
    {

        $this->userRegistrationService = $userRegistrationService;
        $this->router = $router;
        $this->template = $template;
        $this->form = $form;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return HtmlResponse|RedirectResponse
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        /**
         * @var Session $session
         */
        $session = $request->getAttribute('session');
        $this->form->setData($request->getParsedBody());
        if ($request->getMethod() === 'POST' && $this->form->isValid()) {
            $this->userRegistrationService->requestToConfirmEmail($request->getAttribute('email'));
            $session->getSegment('App\Action')->setFlash(
                'flash',
                ['type' => 'success', 'message' => 'The the link to confirm is sent to you email']
            );
            return new RedirectResponse($this->router->generateUri('index'));
        }
        return new HtmlResponse($this->template->render('app::user/email-confirmation-request', [
            'form' => $this->form,
        ]));
    }
}
