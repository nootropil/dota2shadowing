<?php
declare(strict_types=1);

namespace App\Presentation\Action\User;

use App\Core\Domain\Service\User\UserRegistrationServiceInterface;
use App\Core\Infrastructure\Exception\ValidationException;
use Aura\Session\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Whoops\Exception\ErrorException;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router;

class EmailConfirmationAction
{
    /**
     * @var UserRegistrationServiceInterface
     */
    private $userRegistrationService;

    /**
     * @var Router\RouterInterface
     */
    protected $router;

    /**
     * EmailConfirmationAction constructor.
     * @param Router\RouterInterface $router
     * @param UserRegistrationServiceInterface $userRegistrationService
     */
    public function __construct(
        Router\RouterInterface $router,
        UserRegistrationServiceInterface $userRegistrationService
    )
    {

        $this->userRegistrationService = $userRegistrationService;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next
     * @return RedirectResponse
     * @throws ErrorException
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next = null)
    {
        /**
         * @var Session $session
         */
        $session = $request->getAttribute('session');
        $token = $request->getAttribute('token');
        try {
            $this->userRegistrationService->confirmEmail($token);
            $session->getSegment('App\Action')->setFlash(
                'flash',
                ['type' => 'success', 'message' => 'You are successfully registered']
            );
            return new RedirectResponse($this->router->generateUri('index'));
        } catch (ValidationException $exception) {
            throw new ErrorException($exception->getMessage());
        }
    }
}
