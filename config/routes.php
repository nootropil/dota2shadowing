<?php
$app->injectRoutesFromConfig((new \App\ConfigProvider())());
$app->injectRoutesFromConfig((new \Authorization\ConfigProvider())());
$app->injectRoutesFromConfig((new \Authentication\ConfigProvider())());
