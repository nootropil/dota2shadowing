<?php

$directory =  '/var/www/click/data/cache/twig/';
recursiveRemoveDirectory($directory);
function recursiveRemoveDirectory($directory)
{
    foreach(glob("{$directory}/*") as $file)
    {
        if(is_dir($file)) {
            recursiveRemoveDirectory($file);
        } else {
            unlink($file);
        }
    }
    rmdir($directory);
}

exit(0);
